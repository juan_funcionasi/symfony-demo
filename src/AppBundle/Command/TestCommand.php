<?php

namespace AppBundle\Command;

use AppBundle\Manager\ProductManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Question\ChoiceQuestion;

class TestCommand extends Command
{
    protected function configure()
    {
        $this->setName("test:test")
            ->setDescription("Descripción para el comando.")
            ->addArgument('name', InputArgument::REQUIRED, 'Decir hola a...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container = $this->getApplication()->getKernel()->getContainer();

        // -- Ejemplo de acceso al manager para obtener datos de BBDD
        $manager = $this->container->get(ProductManager::class);

        $list = $manager->findAll();

        $output->writeln('');
        $output->writeln('<info>-- LISTADO DE PRODUCTOS Y PRECIO --</info>');
        $output->writeln('');
        foreach ($list as $product) {
            $output->writeln('<question> - ' . $product->getName() . ': '. $product->getPrice() .'€</question>');
        }

        // -- Definición de formato propio.
        $outputStyle = new OutputFormatterStyle('red', 'yellow', array('bold', 'blink'));
        $output->getFormatter()->setStyle('fire', $outputStyle);

        $output->writeln('');
        $output->writeln('<info>-- FORMATO PROPIO --</info>');
        $output->writeln('');
        $output->writeln('<fire>Ejemplo de formato propio.</fire>');


        // -- Mostramos texto utilizando el argumento pasado al ejecutar el comando.
        $text = $input->getArgument('name');
        $output->writeln('');
        $output->writeln('<info>-- EJEMPLO DE PARAMETRO PASADO AL SCRIPT --</info>');
        $output->writeln('');
        $output->writeln('<comment>Hola holita ' . $text . '!!!</comment>');
        $output->writeln('');


        // -- Ejemplo de preguntas al usuario.
        $output->writeln('');
        $output->writeln('<info>-- EJEMPLO DE INTERACCIÓN CON EL USUARIO --</info>');
        $output->writeln('');
        do {
            $helper = $this->getHelper('question');
            $question = new ChoiceQuestion(
                'Selecciona un color. Por defecto será el rojo.',
                array('rojo', 'azul', 'amarillo', 'salir'),
                0
            );
            $question->setErrorMessage('El color no es válido.');

            $color = $helper->ask($input, $output, $question);
            $output->writeln('<error>Has seleccionado: '.$color. '</error>');

        } while($color != 'salir');
    }

}