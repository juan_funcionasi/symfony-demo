<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;
use AppBundle\Manager\ProductManager;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends Controller
{
    /**
     * @Route("/product/list", name="_product_list")
     */
    public function listAction(Request $request)
    {
        $products = $this->get(ProductManager::class)->findAll();

        return $this->render('product/index.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @Route("/product/{id}", name="_product_detail")
     */
    public function detailAction(Request $request, Product $product)
    {
        return $this->render('product/detail.html.twig', [
            'product' => $product
        ]);
    }

    /**
     * @Route("/product/new", name="_product_new")
     */
    public function newAction(Request $request)
    {
        $product = new Product();
        //$product->setName("Hola");

        $form = $this->createForm(ProductType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->get(ProductManager::class)->save($product);

            return $this->redirectToRoute('_product_detail', ['id' => $product->getId()]);
        }

        return $this->render('product/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/product/{id}/edit", name="_product_edit")
     */
    public function editAction(Request $request, Product $product)
    {
        $product->setTags(implode(',', $product->getTags()));

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $this->get(ProductManager::class)->save($product);

            return $this->redirectToRoute('_product_detail', ['id' => $product->getId()]);
        }

        return $this->render('product/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/product/{id}/download", name="_product_download")
     */
    public function downloadAction(Request $request, Product $product)
    {
        $html = $this->renderView('product/detail.html.twig', array(
            'product' => $product,
            'base_dir' => $this->get('kernel')->getRootDir() . '/../web'
        ));

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            'file-product.pdf'
        );
    }

}
