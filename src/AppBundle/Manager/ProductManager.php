<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Product;
use Doctrine\ORM\EntityManager;

class ProductManager
{
    protected $em;
    protected $class;
    protected $repository;

    public function __construct(EntityManager $em, $class)
    {
        $this->em = $em;
        $this->class = $em->getClassMetadata($class)->name;
        $this->repository = $em->getRepository($class);
    }

    public function findAll()
    {
        return $this->repository->findAllOrderedByName();
    }

    /**
     * Get question by id
     * @param integer $id
     * @return Question
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param Product $product
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Product $product)
    {
        if (is_string($product->getTags())) {
            $tags = explode(',', $product->getTags());
            $product->setTags($tags);
        }

        $this->em->persist($product);
        $this->em->flush();
    }

}